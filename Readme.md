#BACKEND DEVELOPMENT FOR WORKOUTWISE APP

This app is an extraordinary effort made by enthusiastic guy.   
The main purpose is to provide all the rest services in the backend side for a gym app. 
However, the idea is to have the whole suite of services needed nowadays for a successful application.  
That is to have a backend in Java, front end in angular and of course, the mobile applications on Android and IOS.


##Tech STACK

-Spring Boot (Web, security, data, devtools, actuator)

-Lombok

-H2 && Flyway

-Swagger


##For execution 

`gradle bootRun
`
