package com.workoutwise.service;

import com.workoutwise.domain.Activity;
import com.workoutwise.domain.User;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.Workout;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by adrian on 5/23/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class OverViewServiceTest {

    @InjectMocks
    OverViewService overViewService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void getTotalActivitiesWhenNoWorkouts() throws Exception {
        UserApp userApp = createUserApp(false);
        int total = overViewService.getTotalOfActivities(userApp);

        assertThat(total, is(0));
    }

    @Test
    public void getTotalActivitiesWhenUserHasWorkouts() throws Exception {
        UserApp userApp = createUserApp(true);
        int total = overViewService.getTotalOfActivities(userApp);

        assertThat(total, is(3));
    }

    private UserApp createUserApp(boolean withActivities){
        return UserApp.builder().user(createUser()).activities(createActivities(withActivities)).build();
    }

    private Set<Activity> createActivities(boolean withActivities){
        Set<Activity> activities1 = new HashSet<>();

        if(withActivities){
            Activity a1 = Activity.builder().name("A1").duration(60).sets(1).repetitions(10).build();
            Activity a2 = Activity.builder().name("A2").duration(120).sets(2).repetitions(10).build();
            Activity a3 = Activity.builder().name("A3").duration(180).sets(3).repetitions(10).build();
            activities1.add(a1);
            activities1.add(a2);
            activities1.add(a3);
        }

        return activities1;
    }

    private User createUser(){
        return User.builder().firstName("Jhon").lastName("snow").activated(true).login("thewalker").build();
    }

}