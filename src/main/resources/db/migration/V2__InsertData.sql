insert into AUTHORITY (NAME) values ('ROLE_ADMIN');
insert into AUTHORITY (NAME) values ('ROLE_USER');

insert into USER (ID,LOGIN,PASSWORD_HASH,FIRST_NAME,LAST_NAME,EMAIL,IMAGE_URL,ACTIVATED,LANG_KEY,CREATED_BY,LAST_MODIFIED_BY) values
(1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','','true','en','system','system');
insert into USER (ID,LOGIN,PASSWORD_HASH,FIRST_NAME,LAST_NAME,EMAIL,IMAGE_URL,ACTIVATED,LANG_KEY,CREATED_BY,LAST_MODIFIED_BY) values
(2,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','','true','en','system','system');
insert into USER (ID,LOGIN,PASSWORD_HASH,FIRST_NAME,LAST_NAME,EMAIL,IMAGE_URL,ACTIVATED,LANG_KEY,CREATED_BY,LAST_MODIFIED_BY) values
(3,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','','true','en','system','system');

insert into USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (1,'ROLE_ADMIN');
insert into USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (2,'ROLE_ADMIN');
insert into USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (3,'ROLE_USER');

insert into USER_APP (ID, WEIGHT, HEIGHT, MUSCLE_PERCENTAGE, FAT_PERCENTAGE, USER_ID) values (1, 75.0, 1.75, 0.0, 0.0, 3);

insert into PHRASE (ID, PHRASE) values (1,'Strive for progress, not perfection');
insert into PHRASE (ID, PHRASE) values (2,'You want me to do something... tell me I can not do it');
insert into PHRASE (ID, PHRASE) values (3,'Strength does not come from physical capacity. It comes from an indomitable will');
insert into PHRASE (ID, PHRASE) values (4,'Motivation will almost always beat mere talent');
insert into PHRASE (ID, PHRASE) values (5,'Energy and persistence conquer all things');
insert into PHRASE (ID, PHRASE) values (6,'Nothing great was ever achieved without enthusiasm');
insert into PHRASE (ID, PHRASE) values (7,'Ability is what you are capable of doing. Motivation determines what you do. Attitude determines how well you do it');
insert into PHRASE (ID, PHRASE) values (8,'Motivation is what gets you started. Habit is what keeps you going');
insert into PHRASE (ID, PHRASE) values (9,'Fear is what stops you... courages is what keeps you going');
insert into PHRASE (ID, PHRASE) values (10,'The finish line is just the beginning of a whole new race');
insert into PHRASE (ID, PHRASE) values (11,'The difference between a goal and a dream is a deadline');
insert into PHRASE (ID, PHRASE) values (12,'Just do it');
insert into PHRASE (ID, PHRASE) values (13,'In seeking happiness for others, you find it for yourself');
insert into PHRASE (ID, PHRASE) values (14,'The secret of getting ahead is getting started');
insert into PHRASE (ID, PHRASE) values (15,'It is not who you are that holds you back, it’s who you think you’re not');
insert into PHRASE (ID, PHRASE) values (16,'Luck is a matter of preparation meeting opportunity');
insert into PHRASE (ID, PHRASE) values (17,'Clear your mind of can’t');
insert into PHRASE (ID, PHRASE) values (18,'It’s never too late to become what you might have been');
insert into PHRASE (ID, PHRASE) values (19,'Fit is Not a Destination, it is a way of living');
insert into PHRASE (ID, PHRASE) values (20,'Wake up, Work out, Kick ass, Repeat');

insert into ACTIVITY (ID, CALORIES, DESCRIPTION, DISTANCE, DURATION, IS_WITH_MACHINE, MUSCLE_GROUP, NAME, REPETITIONS, SETS, TIME_X_REPETITION, WEIGHT_TO_LIFT, USER_APP_ID) values
(1,50, 'Hacer push ups', null, 60, false, 'arms', 'Push ups', 10, 4, 2, null, 1);

insert into HISTORY_ACTIVITY (ID, DURATION, NAME, START_DATE, FINISH_DATE, USER_APP_ID) values (1, 60, 'Running', '2017-06-01 14:00:59', '2017-06-01 14:30:59', 1);
insert into HISTORY_ACTIVITY (ID, DURATION, NAME, START_DATE, FINISH_DATE, USER_APP_ID) values (2, 60, 'Push ups and abs', '2017-06-02 16:00:00', '2017-06-02 17:01:59', 1);
insert into HISTORY_ACTIVITY (ID, DURATION, NAME, START_DATE, FINISH_DATE, USER_APP_ID) values (3, 60, 'Dumbbell shoulder press', '2017-06-03 18:00:59', '2017-06-03 18:50:59', 1);
insert into HISTORY_ACTIVITY (ID, DURATION, NAME, START_DATE, FINISH_DATE, USER_APP_ID) values (4, 60, 'Straight-arm dumbbell pull over', '2017-06-04 07:00:59', '2017-06-04 08:01:59', 1);