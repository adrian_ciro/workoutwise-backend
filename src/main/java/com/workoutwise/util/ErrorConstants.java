package com.workoutwise.util;

public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyfailure";
    public static final String ERR_ACCESS_DENIED = "error.accessdenied";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String ERR_METHOD_NOT_SUPPORTED = "error.methodnotsupported";
    public static final String ERR_INTERNAL_SERVER_ERROR = "error.internalservererror";
    public static final String ERR_LOGIN_ALREADY_EXISTS = "error.loginexists";
    public static final String ERR_EMAIL_ALREADY_EXISTS = "error.emailexists";
    public static final String ERR_WORKOUT_ALREADY_EXISTS = "error.workoutexists";
    public static final String ERR_ACTIVITY_ALREADY_EXISTS = "error.activityexists";


}
