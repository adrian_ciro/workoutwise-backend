package com.workoutwise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackageClasses = { WorkoutwiseApplication.class, Jsr310JpaConverters.class })
@EnableJpaRepositories(basePackages = {"com.workoutwise.repository"})
public class WorkoutwiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkoutwiseApplication.class, args);
	}
}
