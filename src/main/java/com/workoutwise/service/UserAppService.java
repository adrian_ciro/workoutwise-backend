package com.workoutwise.service;

import com.workoutwise.domain.User;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.BodyDto;
import com.workoutwise.repository.UserAppRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing UserApp.
 */
@Service
@Transactional
@Slf4j
public class UserAppService {

    private final UserAppRepository userAppRepository;

    public UserAppService(UserAppRepository userAppRepository) {
        this.userAppRepository = userAppRepository;
    }

    public UserApp getUserByLogin(String login){
        List<UserApp> userApps = userAppRepository.findByUserLogin(login);
        return userApps.isEmpty() ? null : userApps.get(0);
    }

    public UserApp save(User user) {
        UserApp userApp = UserApp.builder().user(user).weight(0.0).height(0.0).fatPercentage(0.0).musclePercentage(0.0).build();
        return save(userApp);
    }

    /**
     * Save a userApp.
     *
     * @param userApp the entity to save
     * @return the persisted entity
     */
    public UserApp save(UserApp userApp) {
        log.debug("Request to save UserApp : {}", userApp);
        UserApp result = userAppRepository.save(userApp);
        return result;
    }

    /**
     *  Get all the userApps.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserApp> findAll(Pageable pageable) {
        log.debug("Request to get all UserApps");
        Page<UserApp> result = userAppRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one userApp by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserApp findOne(Long id) {
        log.debug("Request to get UserApp : {}", id);
        UserApp userApp = userAppRepository.findOne(id);
        return userApp;
    }

    /**
     *  Delete the  userApp by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserApp : {}", id);
        userAppRepository.delete(id);
    }

    public void updateBody(BodyDto bodyDto) {

        UserApp userApp = userAppRepository.findOne(bodyDto.getId());
        if(userApp != null){
            userApp.setWeight(bodyDto.getWeight());
            userApp.setHeight(bodyDto.getHeight());
            userApp.setFatPercentage(bodyDto.getFat());
            userApp.setMusclePercentage(bodyDto.getMuscle());
            userAppRepository.save(userApp);
            log.debug("Body fields updated in UserApp ID: ", bodyDto.getId());
        }
    }
}
