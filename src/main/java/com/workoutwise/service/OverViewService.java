package com.workoutwise.service;

import com.workoutwise.domain.HistoryActivity;
import com.workoutwise.domain.User;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.HistoryActivityDTO;
import com.workoutwise.domain.dto.OverviewDTO;
import com.workoutwise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by adrian on 5/23/17.
 */
@Service
public class OverViewService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhraseService phraseService;


    public OverviewDTO getOverviewData(String username) {
        OverviewDTO overviewDTO = new OverviewDTO();
        Optional<User> user= userRepository.findOneByLogin(username);

        if(user.isPresent() && user.get().getUserApp() != null){
            UserApp userApp = user.get().getUserApp();
            overviewDTO.setWeight(userApp.getWeight());
            overviewDTO.setHeight(userApp.getHeight());
            overviewDTO.setPhrase(phraseService.findPhraseOfTheDay());
            overviewDTO.setHistoryActivities(getHistoryActivities(userApp));
            overviewDTO.setTotalActivities(getTotalOfActivities(userApp));
            overviewDTO.setTotalWorkouts(userApp.getWorkouts().size());
        }
        else{
            return null;
        }
        return overviewDTO;
    }

    protected int getTotalOfActivities(UserApp userApp) {
        return userApp.getActivities().size();
    }

    protected List<HistoryActivityDTO> getHistoryActivities(UserApp userApp) {
        if(userApp.getHistoryActivities().isEmpty()){
            return Collections.emptyList();
        }
        return userApp.getHistoryActivities().stream().sorted().sorted(Comparator.reverseOrder()).limit(4)
                .map(historyActivity -> HistoryActivityDTO.createDTO(historyActivity))
                .collect(Collectors.toList());
    }


}
