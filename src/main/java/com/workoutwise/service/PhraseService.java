package com.workoutwise.service;

import com.workoutwise.domain.Phrase;
import com.workoutwise.repository.PhraseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by adrian on 5/23/17.
 */
@Service
public class PhraseService {

    @Autowired
    private PhraseRepository phraseRepository;

    public String findPhraseOfTheDay(){
        List<Phrase> phrases = phraseRepository.findAll();
        int randomIndex = ThreadLocalRandom.current().nextInt(0, phrases.size());
        return phrases.isEmpty() ? "" : phrases.get(randomIndex).getPhrase();
    }
}
