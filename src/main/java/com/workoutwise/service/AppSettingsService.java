package com.workoutwise.service;

import com.workoutwise.domain.AppSettings;
import com.workoutwise.repository.AppSettingsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing AppSettings.
 */
@Service
@Transactional
@Slf4j
public class AppSettingsService {

    private final AppSettingsRepository appSettingsRepository;

    public AppSettingsService(AppSettingsRepository appSettingsRepository) {
        this.appSettingsRepository = appSettingsRepository;
    }

    /**
     * Save a appSettings.
     *
     * @param appSettings the entity to save
     * @return the persisted entity
     */
    public AppSettings save(AppSettings appSettings) {
        log.debug("Request to save AppSettings : {}", appSettings);
        AppSettings result = appSettingsRepository.save(appSettings);
        return result;
    }

    /**
     *  Get all the appSettings.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AppSettings> findAll(Pageable pageable) {
        log.debug("Request to get all AppSettings");
        Page<AppSettings> result = appSettingsRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one appSettings by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AppSettings findOne(Long id) {
        log.debug("Request to get AppSettings : {}", id);
        AppSettings appSettings = appSettingsRepository.findOne(id);
        return appSettings;
    }

    /**
     *  Delete the  appSettings by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AppSettings : {}", id);
        appSettingsRepository.delete(id);
    }
}
