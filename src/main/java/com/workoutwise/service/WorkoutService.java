package com.workoutwise.service;

import com.workoutwise.domain.Activity;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.Workout;
import com.workoutwise.domain.dto.WorkoutInitDTO;
import com.workoutwise.domain.enumeration.Difficulty;
import com.workoutwise.domain.mapper.WorkoutMapper;
import com.workoutwise.repository.WorkoutRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Workout.
 */
@Service
@Transactional
@Slf4j
public class WorkoutService {

    private final WorkoutRepository workoutRepository;

    public WorkoutService(WorkoutRepository workoutRepository) {
        this.workoutRepository = workoutRepository;
    }

    public WorkoutInitDTO initializeWorkouts(UserApp userApp){
        List<String> difficulties =  Arrays.asList(Difficulty.values()).stream()
                .map(difficulty-> difficulty.name().toLowerCase())
                .collect(Collectors.toList());
        List<String> activities = userApp.getActivities().stream().map(a->a.getName()).collect(Collectors.toList());

        return WorkoutInitDTO.builder().difficulties(difficulties)
                .activities(activities)
                .workouts(WorkoutMapper.parseToWorkoutDto(userApp.getWorkouts()))
                .build();
    }

    public Set<Activity> getSelectedActivities(UserApp userApp, List<String> activities){
        if(StringUtils.isEmpty(activities) || StringUtils.isEmpty(userApp.getActivities())){
            return Collections.EMPTY_SET;
        }

       return userApp.getActivities().stream().filter(activity ->
               activities.stream().anyMatch(act->act.equals(activity.getName())))
               .collect(Collectors.toSet());
    }

    /**
     * Save a workout.
     *
     * @param workout the entity to save
     * @return the persisted entity
     */
    public Workout save(Workout workout) {
        log.debug("Request to save Workout : {}", workout);
        Workout result = workoutRepository.save(workout);
        return result;
    }

    /**
     *  Get all the workouts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Workout> findAll(Pageable pageable) {
        log.debug("Request to get all Workouts");
        Page<Workout> result = workoutRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one workout by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Workout findOne(Long id) {
        log.debug("Request to get Workout : {}", id);
        Workout workout = workoutRepository.findOneWithEagerRelationships(id);
        return workout;
    }

    /**
     *  Delete the  workout by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Workout : {}", id);
        workoutRepository.delete(id);
    }
}
