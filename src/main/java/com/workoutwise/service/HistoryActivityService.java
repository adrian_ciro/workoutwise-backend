package com.workoutwise.service;

import com.workoutwise.domain.HistoryActivity;
import com.workoutwise.repository.HistoryActivityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing HistoryActivity.
 */
@Service
@Transactional
@Slf4j
public class HistoryActivityService {

    private final HistoryActivityRepository historyActivityRepository;

    public HistoryActivityService(HistoryActivityRepository historyActivityRepository) {
        this.historyActivityRepository = historyActivityRepository;
    }

    /**
     * Save a historyActivity.
     *
     * @param historyActivity the entity to save
     * @return the persisted entity
     */
    public HistoryActivity save(HistoryActivity historyActivity) {
        log.debug("Request to save HistoryActivity : {}", historyActivity);
        HistoryActivity result = historyActivityRepository.save(historyActivity);
        return result;
    }

    /**
     *  Get all the historyActivities.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<HistoryActivity> findAll(Pageable pageable) {
        log.debug("Request to get all HistoryActivities");
        Page<HistoryActivity> result = historyActivityRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one historyActivity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public HistoryActivity findOne(Long id) {
        log.debug("Request to get HistoryActivity : {}", id);
        HistoryActivity historyActivity = historyActivityRepository.findOne(id);
        return historyActivity;
    }

    /**
     *  Delete the  historyActivity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete HistoryActivity : {}", id);
        historyActivityRepository.delete(id);
    }
}
