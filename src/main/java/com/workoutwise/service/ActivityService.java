package com.workoutwise.service;

import com.workoutwise.domain.Activity;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.ActivityInitDTO;
import com.workoutwise.domain.enumeration.MuscleGroup;
import com.workoutwise.domain.mapper.ActivityMapper;
import com.workoutwise.repository.ActivityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing Activity.
 */
@Service
@Transactional
@Slf4j
public class ActivityService {

    private final ActivityRepository activityRepository;

    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityInitDTO initializeActivities(UserApp userApp) {
        List<String> muscleList = Arrays.asList(MuscleGroup.values()).stream()
                .map(muscle-> muscle.name().toLowerCase())
                .collect(Collectors.toList());

        return ActivityInitDTO.builder().muscleList(muscleList)
                .activities(ActivityMapper.parseToActivityDto(userApp.getActivities()))
                .build();
    }

    /**
     * Save a activity.
     *
     * @param activity the entity to save
     * @return the persisted entity
     */
    public Activity save(Activity activity) {
        log.debug("Request to save Activity : {}", activity);
        Activity result = activityRepository.save(activity);
        return result;
    }

    /**
     *  Get all the activities.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Activity> findAll(Pageable pageable) {
        log.debug("Request to get all Activities");
        Page<Activity> result = activityRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one activity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Activity findOne(Long id) {
        log.debug("Request to get Activity : {}", id);
        Activity activity = activityRepository.findOne(id);
        return activity;
    }

    /**
     *  Delete the  activity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Activity : {}", id);
        activityRepository.delete(id);
    }


}
