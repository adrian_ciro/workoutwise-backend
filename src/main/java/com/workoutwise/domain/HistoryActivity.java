package com.workoutwise.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A HistoryActivity.
 */
@Entity
@Table(name = "history_activity")
@Getter
@Setter
@EqualsAndHashCode
public class HistoryActivity implements Serializable, Comparable<HistoryActivity> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "duration", nullable = false)
    private Integer duration;

    @Column(name = "muscle_group")
    private String muscleGroup;

    @Column(name = "is_with_machine")
    private Boolean isWithMachine;

    @Column(name = "sets")
    private Integer sets;

    @Column(name = "repetitions")
    private Integer repetitions;

    @Column(name = "time_x_repetition")
    private Integer timeXRepetition;

    @Column(name = "weight_to_lift")
    private Double weightToLift;

    @Column(name = "calories")
    private Double calories;

    @Column(name = "distance")
    private Double distance;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    @NotNull
    @Column(name = "finish_date", nullable = false)
    private LocalDateTime finishDate;

    @ManyToOne
    @JsonIgnore
    private UserApp userApp;

    @Override
    public int compareTo(HistoryActivity historyActivity) {
        return this.getStartDate().compareTo(historyActivity.getStartDate());
    }

    @Override
    public String toString() {
        return "HistoryActivity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", muscleGroup='" + muscleGroup + '\'' +
                ", isWithMachine=" + isWithMachine +
                ", sets=" + sets +
                ", repetitions=" + repetitions +
                ", timeXRepetition=" + timeXRepetition +
                ", weightToLift=" + weightToLift +
                ", calories=" + calories +
                ", distance=" + distance +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                '}';
    }
}
