package com.workoutwise.domain.enumeration;

/**
 * Created by adrian on 9/29/17.
 */
public enum MuscleGroup {
    ARMS, LEGS, CHEST, SHOULDERS, ABS, BACK
}
