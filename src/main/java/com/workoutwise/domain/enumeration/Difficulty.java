package com.workoutwise.domain.enumeration;

/**
 * The Difficulty enumeration.
 */
public enum Difficulty {
    EASY,NORMAL,HARD
}
