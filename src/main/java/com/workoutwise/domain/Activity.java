package com.workoutwise.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Activity.
 */
@Entity
@Table(name = "activity")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "duration", nullable = false)
    private Integer duration;

    @Column(name = "muscle_group")
    private String muscleGroup;

    @Column(name = "is_with_machine")
    private Boolean isWithMachine;

    @Column(name = "sets")
    private Integer sets;

    @Column(name = "repetitions")
    private Integer repetitions;

    @Column(name = "time_x_repetition")
    private Integer timeXRepetition;

    @Column(name = "weight_to_lift")
    private Double weightToLift;

    @Column(name = "calories")
    private Double calories;

    @Column(name = "distance")
    private Double distance;

    @ManyToMany(mappedBy = "activities")
    @JsonIgnore
    private Set<Workout> workouts = new HashSet<>();

    @ManyToOne
    private UserApp userApp;

    public Activity addWorkoutactivity(Workout workout) {
        this.workouts.add(workout);
        workout.getActivities().add(this);
        return this;
    }

    public Activity removeWorkoutactivity(Workout workout) {
        this.workouts.remove(workout);
        workout.getActivities().remove(this);
        return this;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", muscleGroup='" + muscleGroup + '\'' +
                ", isWithMachine=" + isWithMachine +
                ", sets=" + sets +
                ", repetitions=" + repetitions +
                ", timeXRepetition=" + timeXRepetition +
                ", weightToLift=" + weightToLift +
                ", calories=" + calories +
                ", distance=" + distance +
                '}';
    }
}
