package com.workoutwise.domain;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Created by adrian on 5/23/17.
 */
@Entity
@Table
@Getter
@ToString
public class Phrase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String phrase;
}
