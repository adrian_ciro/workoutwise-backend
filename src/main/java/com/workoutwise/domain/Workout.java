package com.workoutwise.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.workoutwise.domain.enumeration.Difficulty;
import lombok.*;

/**
 * A Workout.
 */
@Entity
@Table(name = "workout")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Workout implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty")
    private Difficulty difficulty;

    @ManyToMany
    @JoinTable(name = "workout_activities",
               joinColumns = @JoinColumn(name="workouts_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="activities_id", referencedColumnName="id"))
    private Set<Activity> activities = new HashSet<>();

    @ManyToOne
    private UserApp userApp;

    public Workout addActivities(Activity activity) {
        this.activities.add(activity);
        activity.getWorkouts().add(this);
        return this;
    }

    public Workout removeActivities(Activity activity) {
        this.activities.remove(activity);
        activity.getWorkouts().remove(this);
        return this;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", difficulty=" + difficulty.name() +
                '}';
    }
}
