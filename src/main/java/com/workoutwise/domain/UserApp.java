package com.workoutwise.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UserApp.
 */
@Entity
@Table(name = "user_app")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserApp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "height")
    private Double height;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "fat_percentage")
    private Double fatPercentage;

    @Column(name = "muscle_percentage")
    private Double musclePercentage;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="USER_ID")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "userApp")
    @JsonIgnore
    private Set<AppSettings> appSettings = new HashSet<>();

    @OneToMany(mappedBy = "userApp")
    @JsonIgnore
    private Set<HistoryActivity> historyActivities = new HashSet<>();

    @OneToMany(mappedBy = "userApp")
    @JsonIgnore
    private Set<Workout> workouts = new HashSet<>();

    @OneToMany(mappedBy = "userApp")
    @JsonIgnore
    private Set<Activity> activities = new HashSet<>();

    public UserApp addAppSettings(AppSettings appSettings) {
        this.appSettings.add(appSettings);
        appSettings.setUserApp(this);
        return this;
    }

    public UserApp removeAppSettings(AppSettings appSettings) {
        this.appSettings.remove(appSettings);
        appSettings.setUserApp(null);
        return this;
    }

    public UserApp addHistoryActivity(HistoryActivity historyActivity) {
        this.historyActivities.add(historyActivity);
        historyActivity.setUserApp(this);
        return this;
    }

    public UserApp removeHistoryActivity(HistoryActivity historyActivity) {
        this.historyActivities.remove(historyActivity);
        historyActivity.setUserApp(null);
        return this;
    }

    public UserApp addWorkouts(Workout workout) {
        this.workouts.add(workout);
        workout.setUserApp(this);
        return this;
    }

    public UserApp removeWorkouts(Workout workout) {
        this.workouts.remove(workout);
        workout.setUserApp(null);
        return this;
    }

    public UserApp addActivity(Activity activity) {
        this.activities.add(activity);
        activity.setUserApp(this);
        return this;
    }

    public UserApp removeActivity(Activity activity) {
        this.activities.remove(activity);
        activity.setUserApp(null);
        return this;
    }

    @Override
    public String toString() {
        return "UserApp{" +
                "id=" + id +
                ", height=" + height +
                ", weight=" + weight +
                ", fatPercentage=" + fatPercentage +
                ", musclePercentage=" + musclePercentage +
                ", userId=" + user.getId() +
                '}';
    }
}
