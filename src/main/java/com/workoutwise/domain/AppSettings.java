package com.workoutwise.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A AppSettings.
 */
@Entity
@Table(name = "app_settings")
@Getter
@Setter
public class AppSettings implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String value;

    @NotNull
    @Column(nullable = false)
    private String type;

    @ManyToOne
    @JsonIgnore
    private UserApp userApp;

    @Override
    public String toString() {
        return "AppSettings{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
