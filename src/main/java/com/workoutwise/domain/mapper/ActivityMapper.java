package com.workoutwise.domain.mapper;

import com.workoutwise.domain.Activity;
import com.workoutwise.domain.dto.ActivityDTO;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ActivityMapper {

    public static Activity parseToActivity(ActivityDTO dto){
        return Activity.builder().id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .duration(dto.getDuration())
                .sets(dto.getSets())
                .repetitions(dto.getRepetitions())
                .calories(dto.getCalories())
                .isWithMachine(dto.getIsWithMachine())
                .distance(dto.getDistance())
                .muscleGroup(dto.getMuscle())
                .timeXRepetition(dto.getTimeXRepetition())
                .weightToLift(dto.getWeightToLift())
                .build();
    }

    public static List<ActivityDTO> parseToActivityDto(Set<Activity> activities){
        if(activities.isEmpty()){
            return Collections.emptyList();
        }

        return activities.stream().map(activity -> ActivityDTO.builder().id(activity.getId())
                .name(activity.getName())
                .description(activity.getDescription())
                .calories(activity.getCalories())
                .distance(activity.getDistance())
                .duration(activity.getDuration())
                .isWithMachine(activity.getIsWithMachine())
                .muscle(activity.getMuscleGroup())
                .repetitions(activity.getRepetitions())
                .sets(activity.getSets())
                .timeXRepetition(activity.getTimeXRepetition())
                .weightToLift(activity.getWeightToLift())
                .build())
                .collect(Collectors.toList());
    }
}
