package com.workoutwise.domain.mapper;

import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.BodyDto;

public class BodyMapper {

     public static BodyDto parseToBodyDto(UserApp userApp){
         return BodyDto.builder().id(userApp.getId())
                 .weight(userApp.getWeight())
                 .height(userApp.getHeight())
                 .fat(userApp.getFatPercentage())
                 .muscle(userApp.getMusclePercentage())
                 .build();
     }
}
