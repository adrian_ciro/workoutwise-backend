package com.workoutwise.domain.mapper;

import com.workoutwise.domain.Workout;
import com.workoutwise.domain.dto.WorkoutDTO;
import com.workoutwise.domain.enumeration.Difficulty;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class WorkoutMapper {

    public static Workout parseToWorkout(WorkoutDTO dto){
        return Workout.builder().id(dto.getId()).name(dto.getName())
                                .description(dto.getDescription()).difficulty(getDifficulty(dto.getDifficulty())).build();
    }

    public static List<WorkoutDTO> parseToWorkoutDto(Set<Workout> workouts){
        if(workouts.isEmpty()){
            return Collections.emptyList();
        }

        return workouts.stream().map(workout -> WorkoutDTO.builder().id(workout.getId()).name(workout.getName())
                                    .description(workout.getDescription())
                                    .difficulty(workout.getDifficulty().name().toLowerCase())
                                    .activities(workout.getActivities().stream().map(a->a.getName()).collect(Collectors.toList()))
                                    .build())
                        .collect(Collectors.toList());
    }

    public static Difficulty getDifficulty(String difficulty){

        if(difficulty.equalsIgnoreCase(Difficulty.HARD.name())){
            return Difficulty.HARD;
        }

        if(difficulty.equalsIgnoreCase(Difficulty.NORMAL.name())){
            return Difficulty.NORMAL;
        }

        //By default
        return Difficulty.EASY;
    }

}
