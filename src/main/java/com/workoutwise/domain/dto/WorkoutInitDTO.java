package com.workoutwise.domain.dto;

import lombok.*;

import java.util.List;

/**
 * Created by adrian on 5/23/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkoutInitDTO {

    private List<String> difficulties;
    private List<String> activities;
    private List<WorkoutDTO> workouts;

}
