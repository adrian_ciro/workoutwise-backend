package com.workoutwise.domain.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class FieldErrorDTO implements Serializable {

    private final String objectName;
    private final String field;
    private final String message;

}
