package com.workoutwise.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * View Model for sending a parameterized error message.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class ParameterizedErrorDTO implements Serializable {

    private final String message;
    private final Map<String, String> paramMap;

}
