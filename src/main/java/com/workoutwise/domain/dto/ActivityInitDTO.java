package com.workoutwise.domain.dto;

import lombok.*;

import java.util.List;

/**
 * Created by adrian on 9/29/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActivityInitDTO {

    List<String> muscleList;
    List<ActivityDTO> activities;
}
