package com.workoutwise.domain.dto;

import com.workoutwise.domain.HistoryActivity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by adrian on 6/3/17.
 */
@Getter
@Setter
@NoArgsConstructor
public class HistoryActivityDTO {

    private String name;
    private Integer duration;
    private String muscleGroup;
    private Boolean isWithMachine;
    private Integer sets;
    private Integer repetitions;
    private Integer timeXRepetition;
    private Double weightToLift;
    private Double calories;
    private Double distance;
    private String startDate;
    private String finishDate;

    public static HistoryActivityDTO createDTO(HistoryActivity historyActivity){

        HistoryActivityDTO dto = new HistoryActivityDTO();
        dto.name = historyActivity.getName();
        dto.duration = historyActivity.getDuration();
        dto.muscleGroup = historyActivity.getMuscleGroup();
        dto.isWithMachine = historyActivity.getIsWithMachine();
        dto.sets = historyActivity.getSets();
        dto.repetitions = historyActivity.getRepetitions();
        dto.timeXRepetition = historyActivity.getTimeXRepetition();
        dto.weightToLift = historyActivity.getWeightToLift();
        dto.calories = historyActivity.getCalories();
        dto.distance = historyActivity.getDistance();
        dto.startDate = historyActivity.getStartDate().toString();
        dto.finishDate = historyActivity.getFinishDate().toString();
        return dto;
    }
}
