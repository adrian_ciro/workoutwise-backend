package com.workoutwise.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by adrian on 9/29/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActivityDTO {

    @NotEmpty
    @JsonProperty("id_token")
    private String idToken;
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @NotEmpty
    private Integer duration;
    @NotEmpty
    private String muscle;
    private Boolean isWithMachine;
    private Integer sets;
    private Integer repetitions;
    private Integer timeXRepetition;
    private Double weightToLift;
    private Double calories;
    private Double distance;
    private List<String> workouts;

}
