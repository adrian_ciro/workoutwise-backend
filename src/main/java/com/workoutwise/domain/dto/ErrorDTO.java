package com.workoutwise.domain.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * View Model for transferring error message with a list of field errors.
 */
@Getter
@Setter
@Builder
public class ErrorDTO implements Serializable {

    private final String message;
    private final String description;
    private List<FieldErrorDTO> fieldErrors;

    public void add(String objectName, String field, String message) {
        if (fieldErrors == null) {
            fieldErrors = new ArrayList<>();
        }
        fieldErrors.add(new FieldErrorDTO(objectName, field, message));
    }

}
