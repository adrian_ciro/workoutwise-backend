package com.workoutwise.domain.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BodyDto {

    private Long id;

    private Double height;

    private Double weight;

    private Double fat;

    private Double muscle;
}
