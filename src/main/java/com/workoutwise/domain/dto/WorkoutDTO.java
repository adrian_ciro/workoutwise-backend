package com.workoutwise.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.workoutwise.domain.Workout;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by adrian on 9/21/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkoutDTO {

    @NotEmpty
    @JsonProperty("id_token")
    private String idToken;
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @NotEmpty
    private String difficulty;
    private List<String> activities;


}
