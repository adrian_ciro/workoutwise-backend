package com.workoutwise.domain.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * View Model object for storing the user's key and password.
 */
@Getter
@Setter
@Builder
public class KeyAndPasswordDTO {

    private String key;
    private String newPassword;

}
