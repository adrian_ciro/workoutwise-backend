package com.workoutwise.domain.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by adrian on 5/23/17.
 */
@Getter
@Setter
@NoArgsConstructor
public class OverviewDTO {

    private String phrase;
    private int totalWorkouts;
    private int totalActivities;
    private double weight;
    private double height;
    private List<HistoryActivityDTO> historyActivities;

}
