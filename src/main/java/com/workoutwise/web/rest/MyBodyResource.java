package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.BodyDto;
import com.workoutwise.domain.dto.ErrorDTO;
import com.workoutwise.domain.dto.JWTTokenDTO;
import com.workoutwise.domain.mapper.BodyMapper;
import com.workoutwise.security.jwt.TokenProvider;
import com.workoutwise.service.UserAppService;
import com.workoutwise.util.ErrorConstants;
import com.workoutwise.util.HeaderUtil;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;

/**
 * REST controller for managing myBody page related with userApp.
 */
@RestController
@RequestMapping("/api")
@Log
public class MyBodyResource {

    private static final String ENTITY_NAME = "userApp";
        
    private final UserAppService userAppService;

    private TokenProvider tokenProvider;

    public MyBodyResource(UserAppService userAppService, TokenProvider tokenProvider) {
        this.userAppService = userAppService;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/mybody")
    @Timed
    public ResponseEntity getUserApp(@RequestBody JWTTokenDTO token) {
        log.log(Level.INFO, "REST request to get the body info");

        if (StringUtils.isEmpty(token.getIdToken())) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_ACCESS_DENIED).build());
        }
        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(token.getIdToken()));
        return ResponseEntity.ok(BodyMapper.parseToBodyDto(userApp));
    }

    @PutMapping("/mybody")
    @Timed
    public ResponseEntity updateUserApp(@RequestBody BodyDto bodyDto) {
        log.log(Level.INFO, "REST request to update UserApp ID: {}", bodyDto.getId());

        userAppService.updateBody(bodyDto);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bodyDto.getId().toString())).body(bodyDto.getId());
    }

}
