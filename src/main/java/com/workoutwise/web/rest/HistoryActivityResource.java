package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.HistoryActivity;
import com.workoutwise.service.HistoryActivityService;
import com.workoutwise.util.ResponseUtil;
import com.workoutwise.util.HeaderUtil;
import com.workoutwise.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

/**
 * REST controller for managing HistoryActivity.
 */
@RestController
@RequestMapping("/api")
@Log
public class HistoryActivityResource {

    private static final String ENTITY_NAME = "historyActivity";
        
    private final HistoryActivityService historyActivityService;

    public HistoryActivityResource(HistoryActivityService historyActivityService) {
        this.historyActivityService = historyActivityService;
    }

    /**
     * POST  /history-activities : Create a new historyActivity.
     *
     * @param historyActivity the historyActivity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new historyActivity, or with status 400 (Bad Request) if the historyActivity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/history-activities")
    @Timed
    public ResponseEntity<HistoryActivity> createHistoryActivity(@Valid @RequestBody HistoryActivity historyActivity) throws URISyntaxException {
        log.log(Level.WARNING, "REST request to save HistoryActivity : {}", historyActivity);
        if (historyActivity.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new historyActivity cannot already have an ID")).body(null);
        }
        HistoryActivity result = historyActivityService.save(historyActivity);
        return ResponseEntity.created(new URI("/api/history-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /history-activities : Updates an existing historyActivity.
     *
     * @param historyActivity the historyActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated historyActivity,
     * or with status 400 (Bad Request) if the historyActivity is not valid,
     * or with status 500 (Internal Server Error) if the historyActivity couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/history-activities")
    @Timed
    public ResponseEntity<HistoryActivity> updateHistoryActivity(@Valid @RequestBody HistoryActivity historyActivity) throws URISyntaxException {
        log.log(Level.WARNING, "REST request to update HistoryActivity : {}", historyActivity);
        if (historyActivity.getId() == null) {
            return createHistoryActivity(historyActivity);
        }
        HistoryActivity result = historyActivityService.save(historyActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, historyActivity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /history-activities : get all the historyActivities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of historyActivities in body
     */
    @GetMapping("/history-activities")
    @Timed
    public ResponseEntity<List<HistoryActivity>> getAllHistoryActivities(@ApiParam Pageable pageable) {
        log.log(Level.WARNING, "REST request to get a page of HistoryActivities");
        Page<HistoryActivity> page = historyActivityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/history-activities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /history-activities/:id : get the "id" historyActivity.
     *
     * @param id the id of the historyActivity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the historyActivity, or with status 404 (Not Found)
     */
    @GetMapping("/history-activities/{id}")
    @Timed
    public ResponseEntity<HistoryActivity> getHistoryActivity(@PathVariable Long id) {
        log.log(Level.WARNING, "REST request to get HistoryActivity : {}", id);
        HistoryActivity historyActivity = historyActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(historyActivity));
    }

    /**
     * DELETE  /history-activities/:id : delete the "id" historyActivity.
     *
     * @param id the id of the historyActivity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/history-activities/{id}")
    @Timed
    public ResponseEntity<Void> deleteHistoryActivity(@PathVariable Long id) {
        log.log(Level.WARNING, "REST request to delete HistoryActivity : {}", id);
        historyActivityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
