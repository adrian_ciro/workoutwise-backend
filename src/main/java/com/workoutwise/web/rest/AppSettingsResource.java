package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.AppSettings;
import com.workoutwise.service.AppSettingsService;
import com.workoutwise.util.ResponseUtil;
import com.workoutwise.util.HeaderUtil;
import com.workoutwise.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

/**
 * REST controller for managing AppSettings.
 */
@RestController
@RequestMapping("/api")
@Log
public class AppSettingsResource {

    private static final String ENTITY_NAME = "appSettings";
        
    private final AppSettingsService appSettingsService;

    public AppSettingsResource(AppSettingsService appSettingsService) {
        this.appSettingsService = appSettingsService;
    }

    /**
     * POST  /app-settings : Create a new appSettings.
     *
     * @param appSettings the appSettings to create
     * @return the ResponseEntity with status 201 (Created) and with body the new appSettings, or with status 400 (Bad Request) if the appSettings has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/app-settings")
    @Timed
    public ResponseEntity<AppSettings> createAppSettings(@Valid @RequestBody AppSettings appSettings) throws URISyntaxException {
        log.log(Level.WARNING, "REST request to save AppSettings : {}", appSettings);
        if (appSettings.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new appSettings cannot already have an ID")).body(null);
        }
        AppSettings result = appSettingsService.save(appSettings);
        return ResponseEntity.created(new URI("/api/app-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /app-settings : Updates an existing appSettings.
     *
     * @param appSettings the appSettings to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated appSettings,
     * or with status 400 (Bad Request) if the appSettings is not valid,
     * or with status 500 (Internal Server Error) if the appSettings couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/app-settings")
    @Timed
    public ResponseEntity<AppSettings> updateAppSettings(@Valid @RequestBody AppSettings appSettings) throws URISyntaxException {
        log.log(Level.WARNING, "REST request to update AppSettings : {}", appSettings);
        if (appSettings.getId() == null) {
            return createAppSettings(appSettings);
        }
        AppSettings result = appSettingsService.save(appSettings);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, appSettings.getId().toString()))
            .body(result);
    }

    /**
     * GET  /app-settings : get all the appSettings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of appSettings in body
     */
    @GetMapping("/app-settings")
    @Timed
    public ResponseEntity<List<AppSettings>> getAllAppSettings(@ApiParam Pageable pageable) {
        log.log(Level.WARNING, "REST request to get a page of AppSettings");
        Page<AppSettings> page = appSettingsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/app-settings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /app-settings/:id : get the "id" appSettings.
     *
     * @param id the id of the appSettings to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the appSettings, or with status 404 (Not Found)
     */
    @GetMapping("/app-settings/{id}")
    @Timed
    public ResponseEntity<AppSettings> getAppSettings(@PathVariable Long id) {
        log.log(Level.WARNING, "REST request to get AppSettings : {}", id);
        AppSettings appSettings = appSettingsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appSettings));
    }

    /**
     * DELETE  /app-settings/:id : delete the "id" appSettings.
     *
     * @param id the id of the appSettings to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/app-settings/{id}")
    @Timed
    public ResponseEntity<Void> deleteAppSettings(@PathVariable Long id) {
        log.log(Level.WARNING, "REST request to delete AppSettings : {}", id);
        appSettingsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
