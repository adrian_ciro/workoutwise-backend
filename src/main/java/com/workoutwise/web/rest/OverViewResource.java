package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.dto.OverviewDTO;
import com.workoutwise.service.OverViewService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;

import static com.workoutwise.security.SecurityUtils.getCurrentUserLogin;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Created by adrian on 5/23/17.
 */
@RestController
@RequestMapping("/api")
@Log
public class OverViewResource {

    @Autowired
    private OverViewService overViewService;

    @PostMapping("/overview")
    @Timed
    public ResponseEntity<OverviewDTO> getOverview() throws URISyntaxException {
        log.log(Level.WARNING, "REST request to get Overview");
        String username = getCurrentUserLogin();

        if (isBlank(username)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        OverviewDTO overviewDTO = overViewService.getOverviewData(username);
        return overviewDTO == null ? ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(overviewDTO) : ResponseEntity.ok(overviewDTO);
    }
}
