package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.Workout;
import com.workoutwise.domain.dto.*;
import com.workoutwise.domain.mapper.WorkoutMapper;
import com.workoutwise.security.jwt.TokenProvider;
import com.workoutwise.service.UserAppService;
import com.workoutwise.service.WorkoutService;
import com.workoutwise.util.ErrorConstants;
import com.workoutwise.util.HeaderUtil;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * REST controller for managing Workout.
 */
@RestController
@RequestMapping("/api")
@Log
public class WorkoutResource {

    private static final String ENTITY_NAME = "workout";
        
    private final WorkoutService workoutService;

    private TokenProvider tokenProvider;

    private UserAppService userAppService;

    public WorkoutResource(WorkoutService workoutService, TokenProvider tokenProvider, UserAppService userAppService) {
        this.workoutService = workoutService;
        this.tokenProvider = tokenProvider;
        this.userAppService = userAppService;
    }

    @PostMapping("/workouts")
    @Timed
    public ResponseEntity getInitialInfoForWorkout(@RequestBody JWTTokenDTO token){
        log.log(Level.INFO, "REST request for list Workouts ");

        if (StringUtils.isEmpty(token.getIdToken())) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_ACCESS_DENIED).build());
        }
        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(token.getIdToken()));
        return ResponseEntity.ok(workoutService.initializeWorkouts(userApp));
    }

    @PostMapping("/workout")
    @Timed
    public ResponseEntity createWorkout(@Valid @RequestBody WorkoutDTO workoutDto) {
        log.log(Level.INFO, "REST request to save Workout with name: "+ workoutDto.getName());

        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(workoutDto.getIdToken()));
        if (isWorkoutNameDuplicated(userApp, workoutDto)) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_WORKOUT_ALREADY_EXISTS).build());
        }
        Workout result = saveWorkout(userApp, workoutDto);
        return ResponseEntity.ok(result.getId());
    }

    @PutMapping("/workout")
    @Timed
    public ResponseEntity updateWorkout(@Valid @RequestBody WorkoutDTO workoutDto) {
        log.log(Level.INFO, "REST request to update Workout ID: "+ workoutDto.getId());

        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(workoutDto.getIdToken()));
        if (StringUtils.isEmpty(workoutDto.getId())) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_INTERNAL_SERVER_ERROR).build());
        }
        if (isWorkoutNameDuplicated(userApp, workoutDto)) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_WORKOUT_ALREADY_EXISTS).build());
        }
        Workout result = saveWorkout(userApp, workoutDto);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workoutDto.getId().toString())).body(result.getId());
    }

    @DeleteMapping("/workout/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkout(@PathVariable Long id) {
        log.log(Level.INFO, "REST request to delete Workout ID: "+ id);
        workoutService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    private Workout saveWorkout(UserApp userApp, WorkoutDTO workoutDto) {

        Workout workout = WorkoutMapper.parseToWorkout(workoutDto);
        workout.setActivities(workoutService.getSelectedActivities(userApp, workoutDto.getActivities()));
        workout.setUserApp(userApp);
        return workoutService.save(workout);
    }

    private boolean isWorkoutNameDuplicated(UserApp userApp, WorkoutDTO workoutDto) {
        return userApp.getWorkouts().stream().filter(activity -> activity.getName().compareToIgnoreCase(workoutDto.getName()) == 0
                && activity.getId() != workoutDto.getId()).findFirst().isPresent();
    }

}
