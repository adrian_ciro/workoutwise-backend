package com.workoutwise.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.workoutwise.domain.Activity;
import com.workoutwise.domain.UserApp;
import com.workoutwise.domain.dto.ActivityDTO;
import com.workoutwise.domain.dto.ErrorDTO;
import com.workoutwise.domain.dto.JWTTokenDTO;
import com.workoutwise.domain.mapper.ActivityMapper;
import com.workoutwise.security.jwt.TokenProvider;
import com.workoutwise.service.ActivityService;
import com.workoutwise.service.UserAppService;
import com.workoutwise.util.ErrorConstants;
import com.workoutwise.util.HeaderUtil;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * REST controller for managing Activity.
 */
@RestController
@RequestMapping("/api")
@Log
public class ActivityResource {

    private static final String ENTITY_NAME = "activity";

    private final ActivityService activityService;

    private TokenProvider tokenProvider;

    private UserAppService userAppService;

    public ActivityResource(ActivityService activityService, TokenProvider tokenProvider, UserAppService userAppService) {
        this.activityService = activityService;
        this.tokenProvider = tokenProvider;
        this.userAppService = userAppService;
    }

    @PostMapping("/activities")
    @Timed
    public ResponseEntity getInitialInfoForActivities(@RequestBody JWTTokenDTO token) {
        log.log(Level.INFO, "REST request to get a page of Activities");

        if (StringUtils.isEmpty(token.getIdToken())) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_ACCESS_DENIED).build());
        }
        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(token.getIdToken()));
        return ResponseEntity.ok(activityService.initializeActivities(userApp));

    }

    @PostMapping("/activity")
    @Timed
    public ResponseEntity createActivity(@Valid @RequestBody ActivityDTO activityDTO) {
        log.log(Level.INFO, "REST request to save Activity : ", activityDTO.getName());

        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(activityDTO.getIdToken()));
        if (isActivityNameDuplicated(userApp, activityDTO)) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_ACTIVITY_ALREADY_EXISTS).build());
        }
        Activity result = saveActivity(userApp, activityDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    @PutMapping("/activity")
    @Timed
    public ResponseEntity updateActivity(@Valid @RequestBody ActivityDTO activityDTO) {
        log.log(Level.INFO, "REST request to update Activity ID: ", activityDTO.getId());

        UserApp userApp = userAppService.getUserByLogin(tokenProvider.getLogin(activityDTO.getIdToken()));
        if (StringUtils.isEmpty(activityDTO.getId())) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_INTERNAL_SERVER_ERROR).build());
        }
        if (isActivityNameDuplicated(userApp, activityDTO)) {
            return ResponseEntity.badRequest().body(ErrorDTO.builder().message(ErrorConstants.ERR_ACTIVITY_ALREADY_EXISTS).build());
        }

        Activity result = saveActivity(userApp, activityDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, activityDTO.getId().toString())).body(result);
    }

    @DeleteMapping("/activities/{id}")
    @Timed
    public ResponseEntity<Void> deleteActivity(@PathVariable Long id) {
        log.log(Level.INFO, "REST request to delete Activity ID: ", id);
        activityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    private Activity saveActivity(UserApp userApp, ActivityDTO activityDTO) {
        Activity activity = ActivityMapper.parseToActivity(activityDTO);
        activity.setUserApp(userApp);
        return activityService.save(activity);
    }

    private boolean isActivityNameDuplicated(UserApp userApp, ActivityDTO activityDTO) {
        return userApp.getActivities().stream().filter(activity -> activity.getName().compareToIgnoreCase(activityDTO.getName()) == 0
                && activity.getId() != activityDTO.getId()).findFirst().isPresent();
    }

}
