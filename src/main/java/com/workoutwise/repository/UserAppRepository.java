package com.workoutwise.repository;

import com.workoutwise.domain.UserApp;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserApp entity.
 */
@SuppressWarnings("unused")
public interface UserAppRepository extends JpaRepository<UserApp,Long> {

    List<UserApp> findByUserLogin(String login);
}
