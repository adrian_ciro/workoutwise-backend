package com.workoutwise.repository;

import com.workoutwise.domain.AppSettings;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AppSettings entity.
 */
@SuppressWarnings("unused")
public interface AppSettingsRepository extends JpaRepository<AppSettings,Long> {

}
