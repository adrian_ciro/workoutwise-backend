package com.workoutwise.repository;

import com.workoutwise.domain.Workout;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Workout entity.
 */
@SuppressWarnings("unused")
public interface WorkoutRepository extends JpaRepository<Workout,Long> {

    @Query("select distinct workout from Workout workout left join fetch workout.activities")
    List<Workout> findAllWithEagerRelationships();

    @Query("select workout from Workout workout left join fetch workout.activities where workout.id =:id")
    Workout findOneWithEagerRelationships(@Param("id") Long id);

}
