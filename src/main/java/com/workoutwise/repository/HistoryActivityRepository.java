package com.workoutwise.repository;

import com.workoutwise.domain.HistoryActivity;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the HistoryActivity entity.
 */
@SuppressWarnings("unused")
public interface HistoryActivityRepository extends JpaRepository<HistoryActivity,Long> {

}
