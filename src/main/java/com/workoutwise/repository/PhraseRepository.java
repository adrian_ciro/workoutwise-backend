package com.workoutwise.repository;

import com.workoutwise.domain.Phrase;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by adrian on 5/23/17.
 */
public interface PhraseRepository extends JpaRepository<Phrase,Long> {
}
