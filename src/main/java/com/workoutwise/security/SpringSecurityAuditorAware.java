package com.workoutwise.security;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import static com.workoutwise.util.Constants.SYSTEM_ACCOUNT;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        String userName = SecurityUtils.getCurrentUserLogin();
        return userName != null ? userName : SYSTEM_ACCOUNT;
    }
}
